<#-- @ftlvariable name="" type="sk.fri.uniza.views.LocationView" -->
<!-- calls getPersons().getName() and sanitizes it -->
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <table id="example" class="striped" style="width:100%">
            <thead>
            <tr>
                <th>Location</th>
                <th>Actual Temperature</th>
                <th>Actual Weather</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <#list getMeteoStations() as meteoStations>
                <tr>
                    <td>
                        ${meteoStations.location}
                    </td>
                    <td>
                        <#if getActualWeatherForLocation(meteoStations.getId()).temp gt  373.15>
                            "not information"
                        <#else>
                            "${getActualWeatherForLocation(meteoStations.getId()).temp - 273.15} °C"
                        </#if>

                    </td>
                    <td>
                        ${getActualWeatherForLocation(meteoStations.getId()).description}
                    </td>
                    <td>
                    <a href="weatherDataInfo?id=${meteoStations.getIdToString()}" class="btn waves-effect waves-yellow green"
                       name="action">
                        <i class="material-icons">brightness_4</i>
                    </a>
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>
        <ul class="pagination">
            <#if paged.prevPage?? >
                <li class="waves-effect"><a href="?page=${paged.prevPage}">
                        <i class="material-icons">chevron_left</i></a></li>
            <#else>
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            </#if>

            <#list 1..paged.lastPage as pageNum>
                <#if pageNum == paged.page>
                    <li class="active"><a href="?page=${pageNum}">${pageNum}</a></li>
                <#else>
                    <li class="waves-effect"><a href="?page=${pageNum}">${pageNum}</a></li>
                </#if>
            </#list>

            <#if paged.nextPage?? >
                <li class="waves-effect"><a href="?page=${paged.nextPage}">
                        <i class="material-icons">chevron_right</i></a></li>
            <#else>
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
            </#if>
        </ul>
    </div>
    <br><br>
</div>
<!-- Modal Trigger -->
<#--<a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>-->
