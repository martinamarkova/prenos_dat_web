<#-- @ftlvariable name="" type="sk.fri.uniza.views.WeatherDataView" -->
<!-- calls getPersons().getName() and sanitizes it -->
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <table id="example" class="striped" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Country</th>
                <th>Description</th>
                <th>Temp</th>
                <th>Temp_min</th>
                <th>Temp_max</th>
                <th>Cloudiness</th>
                <th>Pressure</th>
                <th>Humidity</th>
                <th>Wind_speed</th>
                <th>Date</th>

            </tr>
            </thead>
            <tbody>
            <#list getWeatherData() as weatherData>
                <tr>
                    <td>
                        ${weatherData.name}
                    </td>
                    <td>
                        ${weatherData.country}
                    </td>
                    <td>
                        ${weatherData.description}
                    </td>
                    <td>
                        "${weatherData.temp -273.15} °C"
                    </td>
                    <td>
                        "${weatherData.temp_min -273.15} °C"
                    </td>
                    <td>
                        "${weatherData.temp_max -273.15} °C"
                    </td>
                    <td>
                        "${weatherData.cloudiness} %"
                    </td>
                    <td>
                        "${weatherData.pressure} hPa"
                    </td>
                    <td>
                        "${weatherData.humidity} %"
                    </td>
                    <td>
                        "${weatherData.wind_speed} m/s"
                    </td>
                    <td>
                        ${weatherData.added}
                    </td>

                </tr>
            </#list>
            </tbody>
        </table>
        <ul class="pagination">
            <#if paged.prevPage?? >
                <li class="waves-effect"><a href="?page=${paged.prevPage}&id=${getStationID()}">
                        <i class="material-icons">chevron_left</i></a></li>
            <#else>
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            </#if>

            <#list 1..paged.lastPage as pageNum>
                <#if pageNum == paged.page>
                    <li class="active"><a href="?page=${pageNum}&id=${getStationID()}">${pageNum}</a></li>
                <#else>
                    <li class="waves-effect"><a href="?page=${pageNum}&id=${getStationID()}">${pageNum}</a></li>
                </#if>
            </#list>

            <#if paged.nextPage?? >
                <li class="waves-effect"><a href="?page=${paged.nextPage}&id=${getStationID()}">
                        <i class="material-icons">chevron_right</i></a></li>
            <#else>
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
            </#if>
        </ul>
    </div>
    <br><br>
</div>
