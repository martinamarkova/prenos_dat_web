package sk.fri.uniza.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.views.View;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Response;
import sk.fri.uniza.WindFarmDemoApplication;
import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.api.Paged;
import sk.fri.uniza.api.Person;
import sk.fri.uniza.api.WeatherData;
import sk.fri.uniza.auth.Role;
import sk.fri.uniza.auth.Session;
import sk.fri.uniza.auth.Sessions;
import sk.fri.uniza.core.User;
import sk.fri.uniza.views.MeteoView;
import sk.fri.uniza.views.LocationView;
import sk.fri.uniza.views.NewMeteoStationView;
import sk.fri.uniza.views.WeatherDataView;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 *
 */
@Path("/weather")
@Api(value = "Počasie")
public class WeatherResource {

    final Logger myLogger = LoggerFactory.getLogger(this.getClass());
    private Sessions sessionDao;

    public WeatherResource(Sessions sessionDao) {
        this.sessionDao = sessionDao;
    }

    /**
     * ZOZNAM STANIC
     * po zavolani resourcu sa zabezpečí zobrazenie meteostaníc
     * @param user
     * @param uriInfo
     * @param headers
     * @param page
     * @return
     */
    @GET
    @ApiOperation(value = "Zoznam meteo staníc")
    @Produces(MediaType.TEXT_HTML)
    @RolesAllowed({Role.ADMIN, Role.USER_READ_ONLY})
    public View getDeviceTable(@Auth User user, @Context UriInfo uriInfo, @Context HttpHeaders headers, @QueryParam("page") Integer page) {
        String sessionStr = headers.getCookies().get(Sessions.COOKIE_SESSION).getValue();
        Optional<Session> sessionOptional = sessionDao.get(sessionStr);
        sessionOptional.orElseThrow(() -> new WebApplicationException());
        Session session = sessionOptional.get();

        try {
            // Get user info
            Person personLogin = null;
            Response<Person> personResponse = WindFarmDemoApplication.getWindFarmServis().getPerson(session.getBearerToken(), user.getId()).execute();
            if (personResponse.isSuccessful()) {
                personLogin = personResponse.body();
            }

            Response<Paged<List<MeteoStation>>> execute = WindFarmDemoApplication.getWindFarmServis().getPagedMeteoStation("Bearer " + session.getToken(), 10, page).execute();
            if (execute.isSuccessful()) {
                return new MeteoView(uriInfo, execute.body().getData(), execute.body(), personLogin);
            }
            return null;

        } catch (IOException e) {
            e.printStackTrace();
            throw new WebApplicationException(e);
        }

    }

    /**
     * vytvori tabuľku RYCHLEHO POCASIA
     * @param user
     * @param uriInfo
     * @param headers
     * @param page
     * @return
     */
    @Path("/location")
    @GET
    @ApiOperation(value = "Tabuľka zobrazujúca rýchle počasie")
    @Produces(MediaType.TEXT_HTML)
    @RolesAllowed({Role.ADMIN, Role.USER_READ_ONLY})
    public View getDeviceLocationTable(@Auth User user, @Context UriInfo uriInfo, @Context HttpHeaders headers, @QueryParam("page") Integer page) {
        String sessionStr = headers.getCookies().get(Sessions.COOKIE_SESSION).getValue();
        Optional<Session> sessionOptional = sessionDao.get(sessionStr);
        sessionOptional.orElseThrow(() -> new WebApplicationException());
        Session session = sessionOptional.get();

        try {
            // Get user info
            Person personLogin = null;
            Response<Person> personResponse = WindFarmDemoApplication.getWindFarmServis().getPerson(session.getBearerToken(), user.getId()).execute();
            if (personResponse.isSuccessful()) {
                personLogin = personResponse.body();
            }

            Response<List<WeatherData>> weatherResponse= WindFarmDemoApplication.getWindFarmServis().getAllWeatherDataInfo("Bearer " + session.getToken()).execute();
            if (weatherResponse.isSuccessful()) {

            }

            Response<Paged<List<MeteoStation>>> execute = WindFarmDemoApplication.getWindFarmServis().getPagedMeteoStation("Bearer " + session.getToken(), 10, page).execute();
            if (execute.isSuccessful()) {
                return new LocationView(uriInfo, execute.body().getData(), weatherResponse.body(), execute.body(), personLogin);
            }
            return null;

        } catch (IOException e) {
            e.printStackTrace();
            throw new WebApplicationException(e);
        }

    }

    /**
     * vypýta si od backendu informácie o počasí
     * podla parametrov stránkované a podla id stanice
     * @param user
     * @param uriInfo
     * @param headers
     * @param page
     * @param meteoStationId
     * @return
     */
    @Path("/weatherDataInfo")
    @GET
    @ApiOperation(value = "Prehľad celého aktuálneho počasia")
    @Produces(MediaType.TEXT_HTML)
    @RolesAllowed({Role.ADMIN, Role.USER_READ_ONLY})
    public View getWeatherDataInfoTable(@Auth User user, @Context UriInfo uriInfo, @Context HttpHeaders headers, @QueryParam("page") Integer page, @QueryParam("id") Long meteoStationId) { //@QueryParam("id") Long meteoStationId
        String sessionStr = headers.getCookies().get(Sessions.COOKIE_SESSION).getValue();
        Optional<Session> sessionOptional = sessionDao.get(sessionStr);
        sessionOptional.orElseThrow(() -> new WebApplicationException());
        Session session = sessionOptional.get();

        try {
            // Get user info
            Person personLogin = null;
            Response<Person> personResponse = WindFarmDemoApplication.getWindFarmServis().getPerson(session.getBearerToken(), user.getId()).execute();
            if (personResponse.isSuccessful()) {
                personLogin = personResponse.body();
            }

            Response<Paged<List<WeatherData>>> execute = WindFarmDemoApplication.getWindFarmServis().getWeatherDataInfo("Bearer " + session.getToken(), 10, page, meteoStationId).execute();
            if (execute.isSuccessful()) {
                return new WeatherDataView(uriInfo, execute.body().getData(), execute.body(), personLogin);
            }
            return null;

        } catch (IOException e) {
            e.printStackTrace();
            throw new WebApplicationException(e);
        }

    }

    /**
     * zabezpečí vymazanie meteostanice z databázy
     * @param user
     * @param uriInfo
     * @param headers
     * @param stationID
     * @return
     */
    @GET
    @Path("/station-delete")
    @ApiOperation(value = "Vymazanie meteo stanice")
    @Produces(MediaType.TEXT_HTML)
    @RolesAllowed({Role.ADMIN})//@RolesAllowed({Role.USER_READ_ONLY, Role.ADMIN})
    public javax.ws.rs.core.Response userDelete(@Auth User user, @Context UriInfo uriInfo, @Context HttpHeaders headers, @QueryParam("id") Long stationID) {

        if (stationID == null) return null;

        if (!user.getRoles().contains(Role.ADMIN) )
            throw new WebApplicationException(javax.ws.rs.core.Response.Status.UNAUTHORIZED);

        Session session = sessionDao.getSession(headers);

        Response<Void> response;
        try {

            response = WindFarmDemoApplication.getWindFarmServis().deleteMeteoStation(session.getBearerToken(), stationID).execute();
            if (response.isSuccessful()) {
                URI uri = UriBuilder.fromPath("/weather")
                        .queryParam("page", 1)
                        .build();
                return javax.ws.rs.core.Response.seeOther(uri)
                        .build();

            }
            throw new WebApplicationException(response.code());
        } catch (IOException e) {
            e.printStackTrace();
            throw new WebApplicationException(e);
        }

    }

    /**
     * vytvorenie okna kde sa vytvára meteostanica
     * @param user
     * @param uriInfo
     * @param headers
     * @return
     */
    @GET
    @Path("/new-meteoStation")
    @ApiOperation(value = "Vytvorenie okna pre meteo stanicu")
    @Produces(MediaType.TEXT_HTML)
    @RolesAllowed(Role.ADMIN)
    public NewMeteoStationView newMeteostation(@Auth User user, @Context UriInfo uriInfo, @Context HttpHeaders headers) {
        return new NewMeteoStationView(uriInfo, user);
    }

    /**
     * požiadavka na backend o vytvorenie meteostanice
     * @param user
     * @param uriInfo
     * @param headers
     * @param name
     * @param password
     * @param apiKey
     * @return
     */
    @POST
    @Path("/new-meteoStation")
    @ApiOperation(value = "Vytvorenie meteo stanice")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    @RolesAllowed({Role.ADMIN, Role.USER_READ_ONLY})
    public javax.ws.rs.core.Response newMeteostation(@Auth User user, @Context UriInfo uriInfo, @Context HttpHeaders headers,
                                               @NotEmpty @FormParam("name") String name,
                                               @NotEmpty @FormParam("password") String password,
                                               @NotEmpty @FormParam("apiKey") Set<String> apiKey)
    {

        Session session = sessionDao.getSession(headers);
        Response<MeteoStation> meteoStationResponse;
        try {


            //MeteoStation stationToBeSaved = new MeteoStation(name, password, null, null, apiKey, null);


            meteoStationResponse = WindFarmDemoApplication.getWindFarmServis()
                    .saveMeteoStations(session.getBearerToken(), name, password, apiKey)
                    .execute();

            if (!meteoStationResponse.isSuccessful())
                throw new WebApplicationException(meteoStationResponse.errorBody().string(), meteoStationResponse.code());

            URI uri = UriBuilder.fromPath("/weather")
                    .build();

            return javax.ws.rs.core.Response.seeOther(uri)
                    .build();

        } catch (IOException e) {
            e.printStackTrace();
            throw new WebApplicationException(e);
        }

    }


    
}