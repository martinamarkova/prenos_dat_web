package sk.fri.uniza.api;


/**
 * objekt dát o počasí
 */
public class WeatherData {
    private Long idu;
    private Long id;
    private String name;
    private String country;
    private String description;
    private Double temp;
    private Double temp_min;
    private Double temp_max;
    private Integer cloudiness;
    private Integer pressure;
    private Integer humidity;
    private Double wind_speed;
    private String added;

    /**
     * vytvorenie a inicializácia počasia
     * @param idu
     * @param id
     * @param name
     * @param country
     * @param description
     * @param temp
     * @param temp_min
     * @param temp_max
     * @param cloudiness
     * @param pressure
     * @param humidity
     * @param wind_speed
     * @param added
     */
    public WeatherData(Long idu, Long id, String name, String country, String description, Double temp, Double temp_min, Double temp_max, Integer cloudiness, Integer pressure, Integer humidity, Double wind_speed, String added) {
        this.idu = idu;
        this.id = id;
        this.name = name;
        this.country = country;
        this.description = description;
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.cloudiness = cloudiness;
        this.pressure = pressure;
        this.humidity = humidity;
        this.wind_speed = wind_speed;
        this.added = added;
    }

    /**
     * defaultný konštruktor
     */
    public WeatherData() {
    }

    public Long getIdu() {
        return idu;
    }

    public void setIdu(Long idu) {
        this.idu = idu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Double getTemp() {
        return temp;
    }
    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getTemp_min() {
        return temp_min;
    }
    public void setTemp_min(Double temp_min) {
        this.temp_min = temp_min;
    }

    public Double getTemp_max() {
        return temp_max;
    }
    public void setTemp_max(Double temp_max) {
        this.temp_max = temp_max;
    }

    public Integer getCloudiness() {
        return cloudiness;
    }
    public void setCloudiness(Integer cloudiness) {
        this.cloudiness = cloudiness;
    }

    public Integer getPressure() {
        return pressure;
    }
    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    public Integer getHumidity() {
        return humidity;
    }
    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public Double getWind_speed() {
        return wind_speed;
    }
    public void setWind_speed(Double wind_speed) {
        this.wind_speed = wind_speed;
    }

    public String getAdded() {
        return added;
    }
    public void setAdded(String registered) {
        this.added = registered;
    }


}
