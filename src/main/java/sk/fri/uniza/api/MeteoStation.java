package sk.fri.uniza.api;

import java.util.Set;
/**
 *táto trieda sa používa pre reprezentovanie určítej metostanice (zariadenia), ktoré sa pripája na server a odosiela naň
 *dáta o počasí
 *v objekte sú uložené všetky informácie o danej meteostanici
 */
public class MeteoStation {

    private Long id;
    private String name;
    private String password;
    private String location;
    private Set<String> apiKey;
    private String registered;
    private Integer port;

    /**
     * default konštruktor
     */
    public MeteoStation(){}

    /**
     * parametrický konštruktor, ktorý inicializuje objekt MeteoStanice pomocou parametrov
     * @param name
     * @param password
     * @param location
     * @param id
     * @param apiKey
     * @param registered čas registrácie
     * @param port
     */
    public MeteoStation(String name, String password, String location, Long id, Set<String> apiKey, String registered, Integer port){
        this.name = name;
        this.password = password;
        this.location = location;
        this.id = id;
        this.apiKey = apiKey;
        this.registered = registered;
        this.port = port;
    }

    /**
     * parametrický konštruktor, ktorý inicializuje objekt MeteoStanice pomocou parametrov
     * @param name
     * @param password
     * @param location
     * @param id
     * @param apiKey
     * @param port
     */
    public MeteoStation(String name, String password, String location, Long id, Set<String> apiKey, Integer port){
        this.name = name;
        this.password = password;
        this.location = location;
        this.id = id;
        this.apiKey = apiKey;
        this.port = port;
    }

    /**
     * @return názov meteostanice (prihlasovacie meno)
     */
    public String getName() {
        return name;
    }
    /**
     * nastaví názov meteostanice (prihlasovacie meno)
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return prihlasovacie heslo meteostanice
     */
    public String getPassword() {
        return password;
    }
    /**
     * nastaví prihlasovacie heslo meteostanice
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return lokalita meteostanice
     */
    public String getLocation() {
        return location;
    }
    /**
     *nastaví lokalitu meteostanice
     *@param location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return id meteostanice
     */
    public Long getId() {
        return id;
    }
    /**
     * nastaví id meteostanice
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * @return id stanice ako string
     */
    public String getIdToString() {
        return id.toString();
    }

    /**
     * @return apiKlúč meteostanice
     */
    public Set<String> getApiKey() {
        return apiKey;
    }
    /**
     * nastaví apiKľúč
     */
    public void setApiKey(Set<String> apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * @return čas registrácie
     */
    public String getRegistered() {
        return registered;
    }
    /**
     * nastaví čas registrácie
     */
    public void setRegistered(String registered) {
        this.registered = registered;
    }

    /**
     * @return port that is uset by metostation
     */
    public Integer getPort() {
        return port;
    }
    /**
     * nastaví port meteostanice
     */
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * @return API-key in String form
     */
    public String getApiKeytoString() {
        String a = apiKey.toString();
        return a;
    }

}
