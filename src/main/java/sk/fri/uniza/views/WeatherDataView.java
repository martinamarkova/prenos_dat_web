package sk.fri.uniza.views;

import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.api.Paged;
import sk.fri.uniza.api.Person;
import sk.fri.uniza.api.WeatherData;
import sk.fri.uniza.core.User;

import javax.ws.rs.core.UriInfo;
import java.util.List;

public class WeatherDataView extends MaterializePage<MaterializeHeader, MaterializeFooter> {
    private final List<WeatherData> weatherData;
    private final Paged paged;
    private final User loginUser;

    public WeatherDataView(UriInfo uriInfo, List<WeatherData> weatherData, Paged paged, Person loginUser) { //Person loginUser
        super("weatherData_table.ftl", uriInfo, new MaterializeHeader(loginUser, "Aktuálne počasie", true), new MaterializeFooter());
        this.weatherData = weatherData;
        this.paged = paged;
        this.loginUser = loginUser;
}

    public Paged getPaged() {
        return paged;
    }

    public User getLoginUser() {
        return loginUser;
    }

    public List<WeatherData> getWeatherData() {
        return weatherData;
    }


    public String getStationID() {
        return weatherData.get(0).getId().toString();
    }

}
