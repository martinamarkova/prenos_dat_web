package sk.fri.uniza.views;

import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.api.Paged;
import sk.fri.uniza.api.Person;
import sk.fri.uniza.api.WeatherData;
import sk.fri.uniza.core.User;

import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * objekt zobrazenia stánky RYCHLEHO POCASIA
 */
public class LocationView extends MaterializePage<MaterializeHeader, MaterializeFooter> {
    private final List<MeteoStation> meteoStations;
    private final List<WeatherData> weatherData;
    private final Paged paged;
    private final User loginUser;

    /**
     * vytvorí obrazovku pre zobrazenie lokalít s aktalnm počasím
     * @param uriInfo
     * @param meteoStations zoznam všetkých meteostaníc
     * @param weatherData všetky dáta o počasí
     * @param paged
     * @param loginUser
     */
    public LocationView(UriInfo uriInfo, List<MeteoStation> meteoStations, List<WeatherData> weatherData, Paged paged, Person loginUser) { //Person loginUser
        super("location_table.ftl", uriInfo, new MaterializeHeader(loginUser, "Lokalita", true), new MaterializeFooter());
        this.meteoStations = meteoStations;
        this.weatherData = weatherData;
        this.paged = paged;
        this.loginUser = loginUser;
    }

    public Paged getPaged() {
        return paged;
    }

    public User getLoginUser() {
        return loginUser;
    }

    /**
     * vyhľadá najaktuálnejšie počasie pre meteostanicu s daným ID
     * @param stationID
     * @return
     */
    public WeatherData getActualWeatherForLocation(Long stationID) {
        Long maxUid = 0L;
        WeatherData result = null;
        for ( WeatherData data: weatherData ) {
            if (data.getId().equals(stationID)&& data.getIdu() > maxUid) {
                result = data;
                maxUid = data.getIdu();
            }
        }
        if (result == null) {
            return new WeatherData(0L, 0L, "NONE", "NONE", "not information for this location", 9999.99, 9999.99, 9999.99, 9999, 9999, 9999, 9999.99, "NONE");
        }

        return  result;
    }



    public List<MeteoStation> getMeteoStations() {
        return meteoStations;
    }
    public List<WeatherData> getWeatherData() {
        return weatherData;
    }
}
