package sk.fri.uniza.views;

import sk.fri.uniza.core.User;

import javax.ws.rs.core.UriInfo;

/**
 * objekt stránky pre vytváranie užívateľa
 */
public class NewMeteoStationView extends MaterializePage<MaterializeHeader, MaterializeFooter>  {
    private final User loginUser;

    /**
     * konštruktor
     * @param uriInfo         uri info from current location       Instance of footer page part
     */
    public NewMeteoStationView(UriInfo uriInfo, User loginUser) {
        super("new_meteostation.ftl", uriInfo, new MaterializeHeader(loginUser, "Nová metostanica", true), new MaterializeFooter());
        this.loginUser = loginUser;
    }

    /**
     *
     * @return prihlásený užívateľ
     */
    public User getLoginUser() {
        return loginUser;
    }

    /*
    private final User loginUser;
    private final String toastMsg;


    public NewPersonView(UriInfo uriInfo, User loginUser, String toastMsg) {
        super("new_person.ftl", uriInfo, new MaterializeHeader(loginUser, "Nový užívateľ", true), new MaterializeFooter());
        this.loginUser = loginUser;
        this.toastMsg = toastMsg;
    }

    public String getToastMsg() {
        return toastMsg;
    }

    public User getLoginUser() {
        return loginUser;
    }

    public Role getSystemRoles() {
        return Role.getInstance();
    }
     */
}
