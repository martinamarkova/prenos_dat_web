package sk.fri.uniza.views;

import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.api.Paged;
import sk.fri.uniza.api.Person;
import sk.fri.uniza.auth.Role;
import sk.fri.uniza.core.User;

import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * trieda pre zobrazenie tabuľky meteostaníc
 */
public class MeteoView extends MaterializePage<MaterializeHeader, MaterializeFooter> {
    private final List<MeteoStation> meteoStations;
    private final Paged paged;
    private final User loginUser;


    /**
     * konštruktor pre vytvorenie tabuľky staníc
     * @param uriInfo
     * @param meteoStations
     * @param paged
     * @param loginUser
     */
    public MeteoView(UriInfo uriInfo, List<MeteoStation> meteoStations, Paged paged, Person loginUser) { //Person loginUser
        super("meteostation_table.ftl", uriInfo, new MaterializeHeader(loginUser, "Meteo Stanice", true), new MaterializeFooter());
        this.meteoStations = meteoStations;

        this.paged = paged;
        this.loginUser = loginUser;
    }

    public String getADMIN() {
        return Role.getADMIN();
    }
    public Paged getPaged() {
        return paged;
    }

    public User getLoginUser() {
        return loginUser;
    }

    public List<MeteoStation> getMeteoStations() {
        return meteoStations;
    }

}
