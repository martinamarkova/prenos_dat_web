# Prenos dát
### Semestrálna práca 
Cieľom semestrálnej práce bolo navrhnúť systém pozostávajúci z nasledujúcich častí postavených na frameworku dropwizard:
    
- **Windfarmdemo** - predstavuje backend systému ako microservis server, ktorý zahŕňa RESTfull rozhranie a zároveň poskytuje OAUTH2 autentifikaciu a BASIC autentifikáciu a prácu s databázou.

- **WindfarmdemoWEB** - mikroservis, ktorý vytvára prezentačnú časť na báze webovej stránky generovanej na strane servera (server-side web pages) . Webová stránka je tvorená HTML5 komponentami s knižnice materializezss a na strane servera je využitý šablonovací systém freemarker.  

- **SensorEmu** - predstavuje konkrétne zariadenia (meteostanice), ktoré získavajú informácie o počasí z https://openweathermap.org/ a následne tieto informácie preposielajú na backend systému.

### Analýza
   **Schéma navrhnutého systému**:
[![N|Solid](http://imgworld.cz/ukazka/z3uw3thtga.png)](https://nodesource.com/products/nsolid)

Aplikácie sú zhotovené na rozpracovaných projektoch vyučujúceho [Ing.Martina Hudíka PhD.] z repozitárov vyučujúceho: (https://github.com/hudikm)
- [Windfarm demo](https://github.com/hudikm/WindFarmDemoWeb)
- [Windfarm demo web](https://github.com/hudikm/WindFarmDemoWeb)

Požiadavky na dané komponenty navrhovaného systému 

**SensorEmu** (Koncové zariadenie) - ide o samostatnú aplikáciu, ktorá emuluje zariadenia. Maximálny počet zariadení, ktoré je možné spustiť je 8. Ide o emulované meteostanice. Senzor musí komunikovať pomocou REST rozhrania s cloud serverom openweathermap.org a súčasne s backend aplikáciou.


Autor: Martina Marková, František Kubaš, 2019

**Windfarmdemo** (backend)- práca s databázou pomocou restového rozhrania
- zápis a čítanie do databázy
-komunikácia pomocou REST rozhrania s meteostanicami, od meteostanice sa príjmajú dáta o počasí a ukladajú sa do databázy
taktiež poskytuje rozhranie pre aplikáciu windfarm demo web.  
-zápis do databázy sa uskutočnuje až po úspešnej autorizácií zariadenia pomocou api kľúča. 
-Zabezpečuje autentifikáciu zaradení pomocou Basic autentifikácie a Užívateľov pomocou Oauth2 autentifikácie.

**Windfarmdemo web** (frontend) - jedná sa o webové rozhranie, ktoré bude zobrazovať namerané dáta (informácie o počasí) z jednotlivých koncových zariadení, aplikácia dokáže zobrazovať počasie pre jednotlivé stanice osobitne. Poskytuje možnosť pridávať a odoberať meteostanice. Pre pístup na web sa musí užívaťeľ prihlásiť a autentifikovať pomocou Oauth2 autentifikácie, Aplikácia musí poskytovať rest rozhranie pomocou ktoreho komunikuje s backend aplikáciou.

## Návrh riešenia  
Vypracované projekty sú založené na rozpracovaných projektoch **WindFarmDemo**, preto nadväzujú na toto riešenie

Windfarmdemo je aplikácia postavená na frameworku dropwizard, ktorý je poskladaný z nasledujúcich knižníc:
- Jetty HTTP web server.
- Jersey RESTové rozhranie.
- Jackson JSON serializovanie a deserializovanie.
- Logback logovanie.
- Hibernate Validator validovanie dát'
- JDBI a Hibernate databáza
- Liquibase migrácia.
- FreeMarker a mustache generovanie webových stránok

[![N|Solid](https://qph.fs.quoracdn.net/main-qimg-5007c0f8192ea85034e691af5b042eab)](https://nodesource.com/products/nsolid)

## Riešenie:
#### Dôvoležité štruktúry
**Meteostanica** pozostáva z:
    private Long id - identifikačné číslo meteostanice
    private String name - prihlasovacie meno meteostanice
    private String password - prihlasovacie heslo meteostanice
    private String location - lokalizácia meteostanice
    private Set<String> apiKey - apikľúč pomocou, ktorého sa stanica autorizuje 
    private String registered - dátum registrácie meteostanice
    private Integer port - port, na ktorom je daná stanica prístupná 

**Weather data** pozostáva z:
    private Long idu - predstavuje jedinečný identifikátor pre záznam počasia
    private Long id - id meteostanice
    private String name - názov meteostanice
    private String country - skratka krajiny, v ktorej sa meteostanica nachádza 
    private String description - skrátený popis aktuálneho počasia
    private Double temp - aktuálna nameraná teplota [°F]
    private Double temp_min - minimálna nameraná teplota [°F]
    private Double temp_max - maximálna nameraná teplota [°F]
    private Integer cloudiness - informácia predstavujúca aktuálnu oblačnosť [%]
    private Integer pressure - informácia o tlaku [hPa]
    private Integer humidity - informácia o vlhkosti vzduchu [%]
    private Double wind_speed - informácia hovoriaca o rýchlosti vetra [m/s]
    private String added - čas snímania počasia
    
**SensorEmu** - Zariadenie predstavuje Meteostanicu, ktorá v pravidelných intervaloch 15 minút zistí počasie z cloudu. Informácie o počasí následne odošle na Backend. Emulátor je navrhnutý ako samostatná aplikácia, ktorá sa môže spustiť 8-mimi konfiguráiami, čiže dokážeme naraz spustiť až 8 zariadení, pričom každé predstavuje inú metostanicu. Na základe Konfiguračného súboru, s krorým bol program spustený sa meteostanica inicializuje na dané hodnoty.

**Backend** - Aplikácia zabezpečuje fungovanie celého systému. Pomocou Knižnice JDBI zabezpečuje správu databázy. Tiež sa stará o autentifikáciu a autorizáciu meteostaníc a zariadení. Poskytuje Rozhranie pre Web server aj pre meteostanice.

**Frontend** webaplikácia poskytujúca užívateľské rozhranie spolu s backend aplikáciou poskytujú možnosť pridávania a odoberania meteostaníc aj užívateľov. Poskytuje možnosť zobrazenia registrovaných meteostaníc ako aj náhlad aktuálneho počasia a po kliknutí na tlačidlo aj históriu pre danú stanicu. 
